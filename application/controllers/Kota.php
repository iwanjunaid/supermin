<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kota extends CI_Controller {
	public function index() {
		$this->load->vars([
			'menuActive' => 'kota',
			'tabActive' => 'kota',
		]);

		$content = $this->load->view('partial-kota', null, true);

		$this->load->view('layout', [
			'content' => $content,
		]);
	}
}
