<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {
	public function index() {
		$this->load->vars([
			'menuActive' => 'pegawai',
			'tabActive' => 'pegawai',
		]);

		$content = $this->load->view('partial-pegawai', null, true);

		$this->load->view('layout', [
			'content' => $content,
		]);
	}
}
