<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posisi extends CI_Controller {
	public function index() {
		$this->load->vars([
			'menuActive' => 'posisi',
			'tabActive' => 'posisi',
		]);

		$content = $this->load->view('partial-posisi', null, true);

		$this->load->view('layout', [
			'content' => $content,
		]);
	}
}
