<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index() {
		$this->load->vars([
			'menuActive' => 'home',
			'tabActive' => 'home',
		]);

		$content = $this->load->view('partial-home', null, true);

		$this->load->view('layout', [
			'content' => $content,
		]);
	}
}
