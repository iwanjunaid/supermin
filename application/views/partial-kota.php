<section class="content-header">
  <h1>
    Data Kota 
    <small>Manage Data POS</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Data Kota</a></li>
  </ol>
</section>

<section class="content">
  <ul class="nav nav-tabs">
    <li role="presentation">
      <a href="<?php echo base_url(); ?>">Home</a>
    </li>
    <li role="presentation">
      <a href="<?php echo base_url(); ?>pegawai">Data Pegawai</a>
    </li>
    <li role="presentation">
      <a href="<?php echo base_url(); ?>posisi">Data Posisi</a>
    </li>
    <li role="presentation" class="active">
      <a href="<?php echo base_url(); ?>kota">Data Kota</a>
    </li>
  </ul>
  <div>
    <h2>This is "Data Kota"</h2>
  </div>
</section>
